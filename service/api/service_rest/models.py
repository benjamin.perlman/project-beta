from django.db import models

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=200, unique=True)
    def __str__(self):
        return self.vin

class Technician(models.Model):
    tech_name = models.CharField(max_length=200)
    tech_id = models.PositiveIntegerField()


class Service(models.Model):
    
    vin = models.CharField(max_length=17)

    owner = models.CharField(max_length=200)
    appt_reason = models.CharField(max_length=200, null=True, blank=True)
    appt_date = models.DateTimeField()
    finished = models.BooleanField(default=False)    
    vip = models.BooleanField(default=False)    

    technician = models.ForeignKey(
        Technician,
        related_name="Services",
        on_delete=models.CASCADE,
    )

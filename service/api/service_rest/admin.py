from django.contrib import admin

from service_rest.models import AutomobileVO, Service, Technician

# Register your models here.
admin.site.register(Technician)
admin.site.register(Service)
admin.site.register(AutomobileVO)

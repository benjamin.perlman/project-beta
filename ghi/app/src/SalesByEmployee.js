import React from 'react';

class SalesByEmployee extends React.Component {
    constructor(props) {
      super(props)
      this.state = {
        sales_person: '',
        sales_persons: [],
        sale_records: [],
      };
      this.handleSalesPersonChange = this.handleSalesPersonChange.bind(this);
      this.handleSalesPersonsChange = this.handleSalesPersonsChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
    }

    handleSalesPersonChange(event) {
      const value = event.target.value;
      this.setState({ sales_person: value });
    }
    
    handleSalesPersonsChange(event) {
      const value = event.target.value;
      this.setState({ sales_persons: value });
    }

    handleSaleRecordsChange(event) {
        const value = event.target.value;
        this.setState({ sale_records: value });
    }
    
    async componentDidMount() {
        const saleRecordUrl = 'http://localhost:8090/api/sales_record/'
        const saleRecordResponse = await fetch(saleRecordUrl);

        if (saleRecordResponse.ok) {
            const recordsData = await saleRecordResponse.json();


            this.setState({sale_records: recordsData.sale_records});
            console.log('records data: ', recordsData);
        }


        const salesPersonUrl = 'http://localhost:8090/api/sales_person/'
        const salesPersonResponse = await fetch(salesPersonUrl);

        if (salesPersonResponse.ok) {
            const personData = await salesPersonResponse.json();
            console.log('sales person data: ', personData);


            this.setState({sales_persons: personData.sales_person});

        }
    }

    render() {
      return (
        <>
          <div className="row">
            <div>
              <div className="shadow p-4 mt-4">
                <h1>Sales Person History</h1>
                  <div className="mb-3">
                    <select onChange={this.handleSalesPersonChange} required id="name" name="name" value={this.state.sales_persons} multiple={false} className="form-select">
                        <option value="">Choose a Sales Person</option>
                        {this.state.sales_persons && this.state.sales_persons.map(person => {
                                 return (
                                     <option key={person.name} value={person.name}>{person.name}</option>
                                 );
                        })}
                    </select>
                  </div>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Sales Person</th>
                            <th>Customer</th>
                            <th>VIN</th>
                            <th>Sale Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.sale_records.filter(sales => sales.sales_person == this.state.sales_person).map(record => {
                            return (
                                <tr key={record.id}>
                                    <td>{record.sales_person}</td>
                                    <td>{record.customer}</td>
                                    <td>{record.vin}</td>
                                    <td>{record.price}</td>
                                </tr>
                                )
                        })}
                    </tbody>
                </table>
              </div>
            </div>
          </div>
        </>
      )
    }
}

export default SalesByEmployee;

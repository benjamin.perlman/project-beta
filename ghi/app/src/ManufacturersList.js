import React from 'react';

function ManufacturersList({manufacturers}) {
    return (
    <table className="table table-striped">
            <thead>
                <tr>
                <th>Name</th>
          

                </tr>
            </thead>
            <tbody>
                {manufacturers && manufacturers.map(manufacturer => {
                    return (
                        <tr key={manufacturer.id}>
                            <td>{manufacturer.name}</td>
                    
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
  }
  
  export default ManufacturersList;
  
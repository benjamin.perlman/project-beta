import React from 'react';

class AutomobileForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            color: '',
            year:'',
            vin: '',
            model: '',
            models: [],
        };

        this.handleColorChange = this.handleColorChange.bind(this);
        this.handleYearChange = this.handleYearChange.bind(this);
        this.handleModelChange = this.handleModelChange.bind(this);
        this.handleVINChange = this.handleVINChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        //event.preventDefault();
        const data = {...this.state};
        delete data.models
        const createAutomobileUrl = 'http://localhost:8100/api/automobiles/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(createAutomobileUrl, fetchConfig);
        if (response.ok) {
            const newAutomobile = await response.json();
        }

        const cleared = {
            color: '',
            year: '',
            vin: '',
            model: '',
        }
        event.setState(cleared);
    }

    handleColorChange(event) {
        const value = event.target.value;
        this.setState({ color: value });
    }

    handleYearChange(event) {
        const value = event.target.value;
        this.setState({ year: value });
    }

    handleVINChange(event) {
        const value = event.target.value;
        this.setState({ vin: value });
    }

    handleModelChange(event) {
        const value = event.target.value;
        this.setState({ model: value });
    }

    async componentDidMount() {
        const vehicleUrl = 'http://localhost:8100/api/models/';

        const vehicleResponse = await fetch(vehicleUrl);

        if (vehicleResponse.ok) {
            const vehicleData = await vehicleResponse.json();

            this.setState({
                models: vehicleData.models,
            })
        }
    }
    render() {
        return (
          <>
            <div className="row">
              <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                  <h1>Create a new Automobile</h1>
                  <form onSubmit={this.handleSubmit} id="create-automobile-form">
                    <div className="form-floating mb-3">
                        <input onChange={this.handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                        <label htmlFor="color">Color</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleYearChange} placeholder="Year" required type="number" name="year" id="year" className="form-control" />
                        <label htmlFor="year">Year</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleVINChange} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control" />
                        <label htmlFor="vin">VIN</label>
                    </div>
                    <div className="mb-3">
                      <select onChange={this.handleModelChange} placeholder="Model" required id="models" name="models" className="form-select">
                          <option value="">Choose a Model</option>
                          { this.state.models.map(auto => {
                              return (
                                  <option key={auto.id} value={auto.id}>{auto.name}</option>
                              );
                          })}
                      </select>
                    </div>
                    <button className="btn btn-outline-dark">Create</button>
                  </form>
                </div>
              </div>
            </div>
          </>
        )
      }
  }
  
  export default AutomobileForm;
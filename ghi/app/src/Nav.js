import { NavLink } from "react-router-dom";

function Nav() {
  return (
    <header>
      <nav className="navbar navbar-expand-lg navbar-dark bg-success">
        <div className="container-fluid">
          <NavLink className="navbar-brand" to="/">CarCar</NavLink>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" aria-current="page" to="/sales_person/new/">
                  New Sales Employee
                </NavLink>
                <NavLink className="nav-link" aria-current="page" to="/sales_person/">
                  Sales By Employee
                </NavLink>
                               
                <NavLink className="nav-link" aria-current="page" to="/customer/new/">
                  New Customer
                </NavLink>
                <NavLink className="nav-link" aria-current="page" to="/sales_record/new">
                  New Sales Record
                </NavLink>
                <NavLink className="nav-link" aria-current="page" to="/sales_record/">
                  All Sales Records
                </NavLink>
              </li>

              <li>  

              <NavLink className="nav-link" aria-current="page" to="/models/">
                  All Vehicle Models
                </NavLink>


              <NavLink className="nav-link" aria-current="page" to="/manufacturers/">
                  All Vehicle Manufacturers
                </NavLink>
                <NavLink className="nav-link" aria-current="page" to="/autos/">
                  All Automobiles
                </NavLink>
              </li>
              <li className="nav-item">
      
              
                <NavLink className="nav-link" aria-current="page" to="/models/new">
                  Create a Vehicle Model
                </NavLink>
              
                <NavLink className="nav-link" aria-current="page" to="/manufacturers/new">
                  Create a Vehicle Manufacturer
                </NavLink>
                <NavLink className="nav-link" aria-current="page" to="/autos/new/">
                  Create an Automobile
                </NavLink>
                
              </li>
              <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="/tech/new/">
                  New Technician Employee
                </NavLink>
                <NavLink className="nav-link" aria-current="page" to="/appointments/new/">
                  New Service Appointment
                </NavLink>
                <NavLink className="nav-link" aria-current="page" to="/appointments/">
                 Appointment List
                </NavLink>
                <NavLink className="nav-link" aria-current="page" to="/appointments/history/">
                  Vehicle Service History
                </NavLink>
            
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </header>
  );
}

export default Nav;

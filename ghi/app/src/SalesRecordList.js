import React from 'react';

function SalesRecordList({sale_records}) {
    return (
      <>
      <h1>Sales Records</h1>
        <table className="table table-hover table-dark table-striped">
            <thead>
            <tr>
                <th>Sales Person</th>
                <th>Customer</th>
                <th>Automobile VIN</th>
                <th>Price</th>
            </tr>
            </thead>
            <tbody>
                {sale_records && sale_records.map(sales_record => {
                    return (
                    <tr key={sales_record.id}>
                        <td>{ sales_record.sales_person }</td>
                        <td>{ sales_record.customer }</td>
                        <td>{ sales_record.vin }</td>
                        <td>{ sales_record.price}</td>
                    </tr>
                    );
                })}
            </tbody>
        </table>
      </>
    );
  }
  
  export default SalesRecordList;
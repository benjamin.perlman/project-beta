import React from 'react';

class TechForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      eName: '',
      eID: '1',
 
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeName = this.handleChangeName.bind(this);
    this.handleChangeEID = this.handleChangeEID.bind(this);

  }

 
  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    console.log(data)
    data.tech_name = data.eName

    data.tech_id = data.eID
    delete data.eName;

    delete data.eID;
    console.log(data)

    const fetchOptions = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const url = 'http://localhost:8080/api/technicians/';
    const request = await fetch(url, fetchOptions);
    if (request.ok) {
      this.setState({
        eName: '',
        eID: '1',
      });
    }
  }


  handleChangeName(event) {
    const value = event.target.value;
    this.setState({ eName: value });
  }

  handleChangeEID(event) {
    const value = event.target.value;
    this.setState({ eID: value });
  }

  render() {

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Tech Record</h1>
            <form onSubmit={this.handleSubmit} id="addTechForm">
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeName} value={this.state.eName} placeholder="Tech name" required type="text" id="tech_name" className="form-control" />
                <label htmlFor="tech_name">Tech Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeEID} value={this.state.eID} placeholder="Tech ID" required type="number" id="tech_id" className="form-control" />
                <label htmlFor="tech_id">Tech Id</label>
              </div>
            
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default TechForm;

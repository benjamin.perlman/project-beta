

import React from 'react';

class ManufacturersForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
 
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeName = this.handleChangeName.bind(this);

  }

 
  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    console.log(data)
  

    console.log(data)

    const fetchOptions = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',


      },
    };
    const url = 'http://localhost:8100/api/manufacturers/';
    const request = await fetch(url, fetchOptions);
    if (request.ok) {
      this.setState({
        name: '',
      });
    }
  }


  handleChangeName(event) {
    const value = event.target.value;
    this.setState({ name: value });
  }



  render() {

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Manufacturer</h1>
            <form onSubmit={this.handleSubmit} id="addManufacturersForm">
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeName} value={this.state.name} placeholder="name" required type="text" id="name" className="form-control" />
                <label htmlFor="name">Manufacturer Name</label>
              </div>
            
            
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default ManufacturersForm;

import React from 'react';

class ServiceHistoryList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          vin: '',
          services:[],
        };

    this.handleChangeVIN = this.handleChangeVIN.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    }

//     async componentDidMount() {
   
//     const response = await fetch(url);
//     if (response.ok) {
//       const data = await response.json();
//       this.setState({ conferenvinces: data.conferences });
//     }
//   }




  async handleSubmit(event) {
    event.preventDefault();
    console.log("12")
    const data = {...this.state};
    console.log("13")

   // delete data.conferences;
    const VIN = data.vin;
    console.log(VIN)

    const url = `http://localhost:8080/api/services/history/${VIN}/`;
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      console.log(data)
      this.setState({ services: data.services });
    }
    console.log(data.services)
    console.log(data)
    console.log("1")
  }
  
    

  handleChangeVIN(event) {
    const value = event.target.value;
    this.setState({ vin: value });
  }

    render() {

        return (
          <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Check Vehicle Service History</h1>
                <form onSubmit={this.handleSubmit} id="addTechForm">
                  
                <div className="form-floating mb-3">
                    <input onChange={this.handleChangeVIN} value={this.state.vin} placeholder="Vin" required type="text" id="vin" className="form-control" />
                    <label htmlFor="vin">VIN</label>
                  </div>
            </form>
            
          </div>
        </div>
    
      
{/* //div */}

      <table className="table table-striped">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Owner</th>
                    <th>Date</th>
                    <th>Tech Name</th>
                    <th>Appt Reason</th>

                </tr>
            </thead>
            <tbody>
            {/* <td>record.vin</td>
                            <td>record.owner</td> */}
                {this.state.services.map(service => {
                  const date = new Date(service.appt_date)
                    return (
                        <tr key={service.id}>
                            <td>{service.vin}</td>
                            <td>{service.owner}</td>
                            <td>{date.toDateString()}</td>
                            <td>{service.technician.tech_name}</td>
                            <td>{service.appt_reason}</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
        </div>


    );
  }
}

export default ServiceHistoryList;
  

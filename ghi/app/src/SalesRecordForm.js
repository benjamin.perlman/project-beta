import React from 'react';

class SalesRecordForm extends React.Component {
    constructor(props) {
      super(props)
      this.state = {
        price: '',
        customers: [],
        sales_persons: [],
        automobiles: []
      };
      this.handlePriceChange = this.handlePriceChange.bind(this);
      this.handleCustomerChange = this.handleCustomerChange.bind(this);
      this.handleAutomobileChange = this.handleAutomobileChange.bind(this);
      this.handleSalesPersonChange = this.handleSalesPersonChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
      event.preventDefault();
      const data = {...this.state};
      delete data.customers;
      delete data.sales_persons;
      delete data.automobiles;

      const salesRecordUrl = 'http://localhost:8090/api/sales_record/';
      const fetchConfig = {
        method: 'post',
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };
      const response = await fetch(salesRecordUrl, fetchConfig);
      if (response.ok) {
        const newSalesRecord = await response.json();
        console.log(newSalesRecord);

        const automobileUrl = `http://localhost:8100/api/automobiles/${this.state.automobile}/`;
            const automobileFetchConfig = {
                method: 'put',
                body: JSON.stringify({ sold: true }),
                headers: {
                    'Content-Type': 'application/json',
                }
            }

            const automobileResponse = await fetch(automobileUrl, automobileFetchConfig)
            if (automobileResponse.ok) {
                console.log(automobileResponse);
            } else {
                console.error(automobileResponse);
            }

        const cleared = {
          price: '',
          customer: '',
          automobile: '',
          sales_person: '',
        };
        this.setState(cleared);
      }
    }

    handlePriceChange(event) {
      const value = event.target.value;
      this.setState({ price: value });
    }
    
    handleCustomerChange(event) {
      const value = event.target.value;
      this.setState({ customer: value });
    }

    handleAutomobileChange(event) {
        const value = event.target.value;
        this.setState({ automobile: value });
    }
    
    handleSalesPersonChange(event) {
        const value = event.target.value;
        this.setState({ sales_person: value });
    }

    async componentDidMount() {
        const AutomobilesUrl = 'http://localhost:8090/api/automobiles/';
        const sales_personUrl = 'http://localhost:8090/api/sales_person/';
        const customerUrl = 'http://localhost:8090/api/customer/';

        const autosResponse = await fetch(AutomobilesUrl);
        const salesResponse = await fetch(sales_personUrl);
        const customerResponse = await fetch(customerUrl);

        if (autosResponse.ok) {
            if (salesResponse.ok) {
                if (customerResponse.ok) {
                    const automobileData = await autosResponse.json();
                    const sales_personData = await salesResponse.json();
                    const customerData = await customerResponse.json();


                    this.setState({
                        automobiles: automobileData.automobilevo,
                        sales_persons: sales_personData.sales_person,
                        customers: customerData.customers,
                    })
                }
            }
        }
    }
    render() {
      return (
        <>
          <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a new Sales Record</h1>
                <form onSubmit={this.handleSubmit} id="create-sales-record-form">
                  <div className="mb-3">
                    <select onChange={this.handleAutomobileChange} required id="automobile" name="automobile" value={this.state.automobile} className="form-select">
                        <option value="">Choose an Automobile</option>
                        {this.state.automobiles && this.state.automobiles.filter(automobile => automobile.sold === false).map(automobile => {
                                 return (
                                     <option key={automobile.vin} value={automobile.vin}>{automobile.vin}</option>
                                 );
                        })}
                    </select>
                  </div>
                  <div className="mb-3">
                    <select onChange={this.handleCustomerChange} required id="customer" name="customer" value={this.state.customer} className="form-select">
                        <option value="">Choose a Customer</option>
                        {this.state.customers.map(customer => {
                            return (
                                <option key={customer.name} value={customer.id}>{customer.name}</option>
                            );
                        })}
                    </select>
                  </div>
                  <div className="mb-3">
                    <select onChange={this.handleSalesPersonChange} required id="sales_person" name="sales_person" value={this.state.sales_person} className="form-select">
                        <option value="">Choose a Sales Person</option>
                        {this.state.sales_persons.map(sales_person => {
                            return (
                                <option key={sales_person.name} value={sales_person.id}>{sales_person.name}</option>
                            );
                        })}
                    </select>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handlePriceChange} value={this.state.price} placeholder="Price" required type="number" name="price" id="price" className="form-control" />
                    <label htmlFor="price">Price</label>
                  </div>
                  <button className="btn btn-outline-dark">Create</button>
                </form>
              </div>
            </div>
          </div>
        </>
      )
    }
}

export default SalesRecordForm;
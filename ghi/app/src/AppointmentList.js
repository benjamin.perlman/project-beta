import React from 'react';

    class AppointmentList extends React.Component {
        constructor(props) {
          super(props);
          this.state = {
            services: [],
       
          }
    
           
              
           }

         
        
        async componentDidMount() {
            const url = 'http://localhost:8080/api/services/';
            const response = await fetch(url);
            if (response.ok) {
              const data = await response.json();
              const serviceAppt = data.services
              this.setState({ services: serviceAppt });
            }
        }


          async finish(id) {
              const  fetchConfig = {
                    method: "PUT",
                }
    
                const url = `http://localhost:8080/api/services/${id}/`;
                const finish = await fetch(url, fetchConfig);
                if (finish.ok) {
                  console.log("Appt Finished");
                
                }


          }
        async cancel(id) {
           const fetchConfig = {
                method: "DELETE",
            }
            const url = `http://localhost:8080/api/services/${id}/`;
            const cancel = await fetch(url, fetchConfig);
            if (cancel.ok) {
              console.log("Appt Deleted");
            }
        }
        
           


        render() { 
    return (
    <table className="table table-striped">
            <thead>
                <tr>
                <th>VIP</th>
                <th>VIN</th>
                    <th>Owner</th>
                    <th>Date</th>
                    <th>Time</th>

                    <th>Tech Name</th>
                    <th>Appt Reason</th>
                   
                    <th>Finished</th>
                    <th></th>


                </tr>
            </thead>
            <tbody>
            
                {this.state.services.map(service => {
                        const date = new Date(service.appt_date);
                        console.log(service.appt_date)
                        console.log(date)
                        return (

                        <tr key={service.id}>
                        <td className={` ${service.vip ? 'bg-info' : 'text-muted'}`}> {service.vip.toString()} </td>
                        <td>{service.vin}</td>
                        <td>{service.owner}</td>
                        <td>{date.toDateString()}</td>
                        <td>{date.toLocaleTimeString()}</td>

                        <td>{service.technician.tech_name}</td>
                        <td>{service.appt_reason}</td>
                        
                        
                        <td>
                        <button className="btn btn-primary btn-lg "  onClick={() => {this.finish(service.id)}}>Finished</button>
                        </td>
                        <td>
                        <button  className="btn btn-primary btn-lg " onClick={() => {this.cancel(service.id)}}>Cancel</button>
                        </td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
  }
}
  
  export default AppointmentList;
